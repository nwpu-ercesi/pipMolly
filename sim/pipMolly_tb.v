// Copyright 2022 Northwestern Polytechnical University.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.
`timescale 1ns / 1ps

`define TRACE_REF_FILE "golden_reference.dat"
//`define DEBUG_REG_WB_EN dut_ocMolly_dpath.reg_wr_en
//`define DEBUG_REG_WB_IDX dut_ocMolly_dpath.reg_index
//`define DEBUG_REG_WV_DAT dut_ocMolly_dpath.reg_wb
//`define DEBUG_PC_BCH dut_ocMolly_dpath.u_pc_gen.pc_branch

//`define VERDI
module pipMolly_tb(/*AUTOARG*/);
   reg core_clk;
   reg core_rst_b;
   /*AUTOREG*/
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [31:0]		d_mem_addr_o;		// From dut_pipMolly of pipMolly.v
   wire [31:0]		d_mem_data_o;		// From dut_pipMolly of pipMolly.v
   wire			d_mem_oe_b;		// From dut_pipMolly of pipMolly.v
   wire			d_mem_we_b;		// From dut_pipMolly of pipMolly.v
   wire [31:0]		i_mem_addr_o;		// From dut_pipMolly of pipMolly.v
   wire			i_mem_oe_b;		// From dut_pipMolly of pipMolly.v
   // End of automatics
   wire [31:0] 		i_mem_data_i;
   wire [31:0] 		d_mem_data_i;
  
   wire [31:0] 		inst;
   
   
   
   initial
     begin
	core_clk = 1'b0;
	core_rst_b = 1'b0;
	#990;
	core_rst_b = 1'b1;
     end
   // clk settings, every 10 tiks, reverse the clock signal, period = 20 tiks.
   
   always #10 core_clk=~core_clk;

   pipMolly dut_pipMolly(/*AUTOINST*/
			 // Outputs
			 .d_mem_addr_o		(d_mem_addr_o[31:0]),
			 .d_mem_data_o		(d_mem_data_o[31:0]),
			 .d_mem_we_b		(d_mem_we_b),
			 .d_mem_oe_b		(d_mem_oe_b),
			 .i_mem_addr_o		(i_mem_addr_o[31:0]),
			 .i_mem_oe_b		(i_mem_oe_b),
			 // Inputs
			 .d_mem_data_i		(d_mem_data_i[31:0]),
			 .i_mem_data_i		(i_mem_data_i[31:0]),
			 .core_clk		(core_clk),
			 .core_rst_b		(core_rst_b));
   
   
   // Instantiate Instructio ram with initial data from stimulation file.
   idealram_single_port #(
			  .DATA_DEEPTH(512),
			  .ADDR_WIDTH(9)
			  )
   instram_idealram_single_pot (
				// Outputs
				.dat_o		(i_mem_data_i[31:0]),
				// Inputs
				.oe_b		(i_mem_oe_b),
				.we_b		(1'b1),
				.dat_addr	(i_mem_addr_o[10:2]),
				.dat_i		(),
				.ram_clk	(core_clk));

   
   // Instantiate DATA ram
   idealram_single_port #(
			  .DATA_DEEPTH(256),
			  .ADDR_WIDTH(8)
			  ) 
   dataram_idealram_single_port (
				 // Outputs
				 .dat_o			(d_mem_data_i[31:0]),
				 // Inputs
				 .oe_b			(d_mem_oe_b),
				 .we_b			(d_mem_we_b),
				 .dat_addr		(d_mem_addr_o[9:2]),
				 .dat_i			(d_mem_data_o[31:0]),
				 .ram_clk		(core_clk));   
   



   // open the trace file;
   integer trace_ref;
/* -----\/----- EXCLUDED -----\/-----
   initial 
     begin
	trace_ref = $fopen(`TRACE_REF_FILE, "r");
	#500;
	$finish;
	
     end
 -----/\----- EXCLUDED -----/\----- */

   
   //Get reference result in falling edge
   //reg        trace_cmp_flag;
   reg        debug_end;

   reg [31:0] ref_pc;
   reg [31:0] ref_wb_idx;
   reg [31:0] ref_wdata_v;
   reg 	      debug_err;
   reg [7:0]  wb_err_count;
   reg [7:0]  mem_err_count;
   reg [7:0]  bch_err_count;

   
   // Total Error Count
   reg [7:0] err_count;
/* -----\/----- EXCLUDED -----\/-----
   always @(posedge core_clk)
     begin
	#3;
	if(ref_pc == 32'h0000_0070 && !debug_end && core_rst_b)
	  begin
             debug_end <= 1'b1;
             $display("==============================================================");
             $display("Test end!");
             #40;
             $fclose(trace_ref);
	     err_count <= wb_err_count + mem_err_count + bch_err_count;
             if (debug_err)
               begin
		  $display("Fail!!!Total %d errors!",err_count);
               end
             else
               begin
		  //$display("----PASS!!!");
		  $display("                    __  __       _ _         "); 
		  $display("                   |  \/  |     | | |        "); 
		  $display("          ___   ___| \  / | ___ | | |_   _   "); 
		  $display("         / _ \ / __| |\/| |/ _ \| | | | | |  "); 
		  $display("        | (_) | (__| |  | | (_) | | | |_| |  "); 
		  $display("         \___/ \___|_|  |_|\___/|_|_|\__, |  "); 
		  $display("                                      __/ |  "); 
		  $display("         _ _ _ _____              _ _|___/   "); 
		  $display("        | | | |  __ \            | | | |     "); 
		  $display("        | | | | |__) |_ _ ___ ___| | | |     "); 
		  $display("        | | | |  ___/ _` / __/ __| | | |     "); 
		  $display("        |_|_|_| |  | (_| \__ \__ \_|_|_|     "); 
		  $display("        (_|_|_)_|   \__,_|___/___(_|_|_)     ");
               end
	     $finish;
	  end // if (ref_pc == 32'h0000_0070 && !debug_end)
     end // always @ (posedge core_clk)
 -----/\----- EXCLUDED -----/\----- */
   


   initial 
     begin
	
//	$vcdpluson;//dve waveform
//	$vcdplusmemon;//dve memory dump


	
	$fsdbDumpfile("pipMolly.fsdb");
	$fsdbDumpvars(0);
	$fsdbDumpMDA;
	$fsdbDumpon;
	#5000;
	$finish;
	
     end // initial begin
  
         
endmodule // pipMolly_tb


// Local Variables:
// verilog-library-directories:("." "../src" "model")
// End:
