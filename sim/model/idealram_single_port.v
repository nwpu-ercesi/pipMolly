// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

module idealram_single_port
  #(
    parameter DATA_DEEPTH=128,
    parameter ADDR_WIDTH=8
    )(/*AUTOARG*/
   // Outputs
   dat_o,
   // Inputs
   oe_b, we_b, dat_addr, dat_i, ram_clk
   );
	
   input oe_b;
   input we_b;
   input [ADDR_WIDTH-1:0] dat_addr;
   input [31:0] dat_i;
   input 	ram_clk;
   
   output [31:0] dat_o;

   /*AUTOREG*/
   /*AUTOWIRE*/
   reg [31:0] 	 spram[DATA_DEEPTH-1:0];
   wire [31:0] 	 dat_o;
   

   initial 
     begin
	$readmemh("init.dat",spram,0);
     end

   always @(posedge ram_clk)
     begin
	if (~oe_b && ~we_b)
	  spram[dat_addr] <= dat_i;
     end

   assign dat_o = oe_b?32'bx:spram[dat_addr];
   
endmodule // idealram_single_port

