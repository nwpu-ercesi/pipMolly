// Copyright 2022 Northwestern Polytechnical University.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

module pc_gen(/*AUTOARG*/
   // Outputs
   npc_o,
   // Inputs
   inst_imm16, npc_sel, is_equal, coreclk, core_rst_b
   );

   input [15:0] inst_imm16;
   input 	npc_sel;
   input 	is_equal;

   input 	coreclk;
   input 	core_rst_b;

   output [31:0] npc_o;

   /*AUTOREG*/
   reg [31:0]	 npc_reg;
   
   /*AUTOWIRE*/
   //wire [31:0]	 pc_four;
   wire [31:0] 		pc_branch;
   reg [31:0] 		init_addr;
   reg 			rst_reg;
   always @(posedge coreclk or core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     init_addr <= 32'b0;
	     rst_reg <= 1'b0;
	  end
	else if(~rst_reg)
	  rst_reg <= 1'b1;
     end
   

   always @(posedge coreclk or core_rst_b)
     begin
	if(~core_rst_b)
	  npc_reg <= 32'b0;
	else if (~rst_reg)
	  npc_reg <= init_addr;
	else
       	  npc_reg <= npc_reg + 32'b0000000_000000000_00000000_00000100;
     end

   assign pc_branch = npc_reg + {npc_o[31:28],inst_imm16[15:0],2'b00};
   assign npc_o = (is_equal && ~npc_sel)?pc_branch:npc_reg;
   
   
        

endmodule
   
   
