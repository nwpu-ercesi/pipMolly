// Copyright 2022 Northwestern Polytechnical University.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

`define DCD_REG_WIDTH 19
module pipMolly(/*AUTOARG*/
   // Outputs
   d_mem_addr_o, d_mem_data_o, d_mem_we_b, d_mem_oe_b, i_mem_addr_o,
   i_mem_oe_b,
   // Inputs
   d_mem_data_i, i_mem_data_i, core_clk, core_rst_b
   );

   //input	 npc_sel;
   //input	 reg_dest_sel;
   //input	 reg_wr_en;
   //input	 ext_op_sel;
   //input	 alu_src_sel;
   //input [3:0] 	 alu_ctr;
   //input 	 mem_wr_en;
   //input 	 memtoreg_sel;
   input [31:0]  d_mem_data_i;
   //input 	 inst_type_sel;
   input [31:0]  i_mem_data_i;
   

   input 	 core_clk;
   input 	 core_rst_b;
   
   //output 	 alu_equal;
   output [31:0] d_mem_addr_o;
   output [31:0] d_mem_data_o;
   output 	 d_mem_we_b;
   output 	 d_mem_oe_b;
   output [31:0] i_mem_addr_o;
   output 	 i_mem_oe_b;

   /*AUTOREG*/
   reg 		 pip_stall;
   reg [31:0] 	 dcd_mem_wr_reg;
   reg [`DCD_REG_WIDTH-1:0] exe_reg;
   reg [31:0] 		    exe_mem_wr_reg;
   reg [31:0] 		    exe_inst_reg;
   reg [31:0] 		    mem_data_reg;
   reg [31:0] 		    mem_reg;
   reg [31:0] 		    mem_alu_o_reg;
   reg 			    valid_if_reg;
   reg 			    valid_id_reg;
   reg 			    valid_ex_reg;
   reg 			    valid_mem_reg;
   
   wire [31:0]		reg_wb;
   wire [31:0]		i_mem_addr_o;
   wire 		i_mem_oe_b;
   
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [3:0]		alu_ctr;		// From u_decode_6inst of decode_6inst.v
   wire			invalid_inst;		// From u_decode_6inst of decode_6inst.v
   wire [31:0]		npc_o;			// From u_pc_gen of pc_gen.v
   wire			npc_sel;		// From u_decode_6inst of decode_6inst.v
   // End of automatics
   
   wire [15:0] 		inst_imm16;
   wire [4:0] 		reg_src;
   wire [4:0] 		reg_tgt;
   wire [4:0] 		reg_dest;

   wire [31:0] 		alu_op2_i;
   wire [31:0] 		alu_op1_i;
   wire [31:0] 		extend_imm;
   wire 		mem_exp;
   
   
   //assign inst_imm16 = i_mem_data_i[15:0];
   //assign reg_src = i_mem_data_i[25:21];
   //assign reg_tgt = i_mem_data_i[20:16];
   //assign reg_dest = i_mem_data_i[15:11];

   pc_gen u_pc_gen(/*AUTOINST*/
		   // Outputs
		   .npc_o		(npc_o[31:0]),
		   // Inputs
		   .inst_imm16		(inst_imm16[15:0]),
		   .npc_sel		(npc_sel),
		   .is_equal		(is_equal),
		   .coreclk		(coreclk),
		   .core_rst_b		(core_rst_b));

   //TODO: pc_gen with branch modified.
   assign coreclk = core_clk;
   
   //**********Instruction Fetch stage

   reg [31:0] 		ir_if_reg;
   
   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  ir_if_reg <= 32'b0;
	else if(~pip_stall)
	  ir_if_reg <= i_mem_data_i;
     end

   // Instruction memory accessing. i_mem_ob_b is reserved as a instruction memory
   // accesssing control for futher Van-Neumann O3 Arch.
   assign i_mem_oe_b = 1'b0;
   assign i_mem_addr_o = i_mem_oe_b ? 32'bx : npc_o;

   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     /*AUTORESET*/
	     // Beginning of autoreset for uninitialized flops
	     valid_if_reg <= 1'h0;
	     // End of automatics
	  end
	else if(~pip_stall)
	  begin
	    valid_if_reg <= 1'b1;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)
   
   //**********End of Instruction Fetch Stage ************//
   
   //**********Data Fetch & Decode Stage******************//
   reg [31:0] reg_file[31:0];
   wire [4:0] reg_index;

   //wire       npc_sel; //npc selection, 1: pc+4, 0: pc+4+imm16||00
   wire       alu_src_sel;//ALU input selection, 1:alu_op2_reg from reg file, 0:alu_op2_reg from imm
   wire	      ext_op_sel;//IMM extension selection, 1: signed ext, 0: zero ext.

   reg [31:0] alu_op1_reg;
   reg [31:0] alu_op2_reg;
   reg [3:0]  alu_ctr_dcd_reg;

   wire       mem_wr_en;
   
   wire       reg_dest_sel; //Selection of reg file write inputs,
                                    //1: rd, 0: rt
   wire	      reg_wr_en;//Reg file write enable, 1 for a enable write
   wire       memtoreg_sel;

   wire [31:0] inst;
   
   reg [`DCD_REG_WIDTH-1:0]  dcd_reg;
   reg [31:0] 		     dcd_inst_reg;

   assign inst = ir_if_reg;

   decode_6inst u_decode_6inst(/*AUTOINST*/
			       // Outputs
			       .npc_sel		(npc_sel),
			       .reg_dest_sel	(reg_dest_sel),
			       .reg_wr_en	(reg_wr_en),
			       .ext_op_sel	(ext_op_sel),
			       .alu_src_sel	(alu_src_sel),
			       .alu_ctr		(alu_ctr[3:0]),
			       .mem_wr_en	(mem_wr_en),
			       .memtoreg_sel	(memtoreg_sel),
			       .invalid_inst	(invalid_inst),
			       // Inputs
			       .inst		(inst[31:0]));
   

   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     dcd_reg <= 0;
	  end
	else if(valid_if_reg) //from pipline control logic
	  begin
	     //mem_wr_en_dcd_reg <= mem_wr_en;
	     //reg_dest_sel_dcd_reg <= reg_dest_sel;
	     //reg_wr_en_dcd_reg <= reg_wr_en;
	     //memtoreg_sel_dcd_reg <= memtoreg_sel;
	     dcd_reg <= {reg_dest_sel, reg_wr_en, memtoreg_sel, mem_wr_en, 
			 reg_src, reg_tgt, reg_dest};
	     
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)

   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     dcd_inst_reg <= 32'b0;
	  end
	else if(~pip_stall)
	  begin
	     dcd_inst_reg <= ir_if_reg;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)

   assign inst_imm16 = ir_if_reg[15:0];
   assign reg_src = ir_if_reg[25:21];
   assign reg_tgt = ir_if_reg[20:16];
   assign reg_dest = ir_if_reg[15:11];
   // alu reg_file read
   assign alu_op1_i = reg_file[reg_src];
   assign reg2_o = reg_file[reg_tgt];

   // extention for imm16, if ext_op_sel=1, it will excute signed extend

   assign extend_imm = ext_op_sel?{{16{inst_imm16[15]}},inst_imm16}:{16'b0,inst_imm16};

   // ALU input selection. If alu_src_sel = 1, it will select data from reg file.
   assign alu_op2_i = alu_src_sel?reg2_o:extend_imm;

   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     alu_op1_reg <= 32'b0;
	     alu_op2_reg <= 32'b0;
	     alu_ctr_dcd_reg <= 4'b0;
	     dcd_mem_wr_reg <= 32'b0;
	  end
	else if(valid_if_reg)
	  begin
	     alu_op1_reg <= alu_op1_i;
	     alu_op2_reg <= alu_op2_i;
	     alu_ctr_dcd_reg <= alu_ctr;
	     dcd_mem_wr_reg <= reg2_o;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)

   assign is_equal = (reg_file[reg_src] == reg_file[reg_tgt])?1'b1:1'b0;
   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     /*AUTORESET*/
	     // Beginning of autoreset for uninitialized flops
	     valid_id_reg <= 1'h0;
	     // End of automatics
	  end
	else if((valid_if_reg)&&(~invalid_inst))
	  begin
	     valid_id_reg <= 1'b1;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)
	  
   //*******End of Data Fetch & Decode Stage********

   
   //*******Start of Excution Stage*****************
   wire [31:0] alu_o;
   reg [31:0]  alu_o_reg;
   
   ALU u_alu(
	     // Outputs
	     .alu_o			(alu_o[31:0]),
	     .overflow                  (overflow),
	     // Inputs
	     .alu_op1_i			(alu_op1_reg[31:0]),
	     .alu_op2_i			(alu_op2_reg[31:0]),
	     .alu_ctr			(alu_ctr_dcd_reg[3:0]));
   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_clk)
	  begin
	     alu_o_reg <= 32'b0;
	     exe_reg <= 0;
	     exe_inst_reg <= 32'b0;
	     exe_mem_wr_reg <= 32'b0;
	  end
	else if(valid_id_reg)
	  begin
	     alu_o_reg <= alu_o;
	     exe_reg <= dcd_reg;
	     exe_inst_reg <= dcd_inst_reg;
	     exe_mem_wr_reg <= dcd_mem_wr_reg;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)
   always @(posedge core_clk or negedge core_rst_b) begin
      if (~core_rst_b) begin
	 /*AUTORESET*/
	 // Beginning of autoreset for uninitialized flops
	 valid_ex_reg <= 1'h0;
	 // End of automatics
      end else if((valid_id_reg) && ~overflow) begin
	 valid_ex_reg <= 1'b1;
      end
   end
   
   //*******End of Excution Stage*****************

   //*******Start of DataMem Access Stage*********
   assign d_mem_we_b = ~exe_reg[15];
   assign d_mem_oe_b = exe_reg[16] && ~exe_reg[15];   
   assign d_mem_addr_o = alu_o_reg;
   assign d_mem_data_o = exe_mem_wr_reg;
   assign mem_exp = 1'b0;

   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     mem_data_reg <= 32'b0;
	     mem_reg <= 0;
	     mem_alu_o_reg <= 32'b0;
	  end
	else if(valid_ex_reg)
	  begin
	     mem_data_reg <= d_mem_data_i;
	     mem_reg <= exe_reg;
	     mem_alu_o_reg <= alu_o_reg;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)
   always @(posedge core_clk or negedge core_rst_b) begin
      if (~core_rst_b) begin
	 /*AUTORESET*/
	 // Beginning of autoreset for uninitialized flops
	 valid_mem_reg <= 1'h0;
	 // End of automatics
      end else if((valid_ex_reg) && ~mem_exp) begin
	 valid_mem_reg <= 1'b1;
      end
   end
   
   //*******End of DataMem Access Stage***********//
    
   //*******Start of Write Back Stage*************//
   assign reg_index = mem_reg[18]?mem_reg[4:0]:mem_reg[9:5];
   assign reg_wb = mem_reg[16]?mem_alu_o_reg:(d_mem_oe_b?32'bx:mem_data_reg);
   
   always @(posedge core_clk or negedge core_rst_b)
     begin
	if(~core_clk)
	  begin
	     reg_file[5'b00000] <= 32'b0;
	  end
	else if (valid_mem_reg && mem_reg[17] && reg_index != 5'b00000)
	  begin
	     reg_file[reg_index] <= reg_wb;
	  end
     end // always @ (posedge core_clk or negedge core_rst_b)
   
   //*******End of Write Back Stage*************//

   //******Start of Pipeline Control*************//

   always @(posedge core_clk or core_rst_b)
     begin
	if(~core_rst_b)
	  begin
	     pip_stall <= 0;
	  end
	else
	  begin
	     pip_stall <= 1'b1;
	  end
     end // always @ (posedge core_clk or core_rst_b)
   
endmodule // pipMolly
