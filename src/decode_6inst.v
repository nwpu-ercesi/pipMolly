// Copyright 2022 Northwestern Polytechnical University.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

module decode_6inst(/*AUTOARG*/
   // Outputs
   npc_sel, reg_dest_sel, reg_wr_en, ext_op_sel, alu_src_sel, alu_ctr,
   mem_wr_en, memtoreg_sel, invalid_inst,
   // Inputs
   inst
   );
   input [31:0] inst;

   output 	 npc_sel;
   output 	 reg_dest_sel;
   output 	 reg_wr_en;
   output 	 ext_op_sel;
   output 	 alu_src_sel;
   output [3:0]  alu_ctr;
   output 	 mem_wr_en;
   output 	 memtoreg_sel;
   output 	 invalid_inst;
   

   /*AUTOREG*/
   // Beginning of automatic regs (for this module's undeclared outputs)
   reg [3:0]		alu_ctr;
   reg			alu_src_sel;
   reg			ext_op_sel;
   reg			invalid_inst;
   reg			mem_wr_en;
   reg			memtoreg_sel;
   reg			npc_sel;
   reg			reg_dest_sel;
   reg			reg_wr_en;
   // End of automatics

   /*AUTOWIRE*/

   always @(*)
     begin
	case (inst[31:26])
	   6'b000000:
	     begin
	        npc_sel = 1'b1;
		reg_dest_sel = 1'b1;
		reg_wr_en = 1'b1;
		alu_src_sel = 1'b1;
		mem_wr_en = 1'b0;
		memtoreg_sel = 1'b1;
		invalid_inst = 1'b0;
		if (inst[5:0] == 6'b100000)//add
		  alu_ctr[3:0] = 4'b0000;
		else if (inst[5:0] == 6'b100010)//sub
		  alu_ctr[3:0] = 4'b0001;
		else
		  alu_ctr[3:0] = 4'b1111;
	     end // case: 6'b000000
	   6'b001101: //ori
	     begin
		npc_sel = 1'b1;
		reg_dest_sel = 1'b0;
		reg_wr_en = 1'b1;
		ext_op_sel = 1'b0;
		alu_src_sel = 1'b0;
		mem_wr_en = 1'b0;
		memtoreg_sel = 1'b1;
		alu_ctr[3:0] = 4'b0010;
		invalid_inst = 1'b0;
	     end
	   6'b100011://lw
	     begin
		npc_sel = 1'b1;
		reg_dest_sel = 1'b0;
		reg_wr_en = 1'b1;
		ext_op_sel = 1'b1;
		alu_src_sel = 1'b0;
		mem_wr_en = 1'b0;
		memtoreg_sel = 1'b0;
		alu_ctr[3:0] = 4'b0000;	
		invalid_inst = 1'b0;
	     end
	   6'b101011://sw
	     begin
		npc_sel = 1'b1;
		reg_wr_en = 1'b0;
		ext_op_sel = 1'b1;
		alu_src_sel = 1'b0;
		mem_wr_en = 1'b1;
		memtoreg_sel = 1'b0;
		alu_ctr[3:0] = 4'b0000;
		invalid_inst = 1'b0;
	     end
	   6'b000100://beq
	     begin
		npc_sel = 1'b0;
		reg_wr_en = 1'b0;
		ext_op_sel = 1'b1;
		alu_src_sel = 1'b1;
		mem_wr_en = 1'b0;
		alu_ctr[3:0] = 4'b0001;
		invalid_inst = 1'b0;
	     end
	  default:
	    begin
	       npc_sel = 1'b0;
	       reg_wr_en = 1'b0;
      	       ext_op_sel = 1'b1;
	       alu_src_sel = 1'b0;
	       mem_wr_en = 1'b0;
	       alu_ctr[3:0] = 4'b1111;
	       invalid_inst = 1'b1;
	    end
	  
	 endcase // case (inst[31:26])
     end // always @ (*)
   
endmodule // decode_6inst
