// Copyright 2022 Northwestern Polytechnical University.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the "License"); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

`define ALU_ADD 4'b0000
`define ALU_SUB 4'b0001
`define ALU_OR 4'b0010

module ALU(/*AUTOARG*/
   // Outputs
   alu_o, overflow,
   // Inputs
   alu_op1_i, alu_op2_i, alu_ctr
   );
   input [31:0] alu_op1_i;
   input [31:0] alu_op2_i;
   input [3:0] 	alu_ctr;

   output [31:0] alu_o;
   output 	 overflow;
   
   //output 	 is_equal;
   

   /*AUTOWIRE*/
   wire [31:0] 	 alu_op1_i;
   wire [31:0] 	 alu_op2_i;
   wire [3:0] 	 alu_ctr;
   wire [31:0] 	 alu_o;
   wire 	 is_equal;
   
   wire [31:0] 	 alu_or_o;
   wire 	 is_sub;
   wire [32:0] 	 math_op1;
   wire [32:0] 	 math_op2;
   wire [32:0] 	 add_re;
   wire [32:0] 	 sub_re;
 	 

   
// alu_ctr  | ALU function 
//     0000 |  add
//     0001 |  sub
//     0010 |  or
//     
   //assign alu_or_o = alu_op1_i | alu_op2_i;
   //assign alu_add_sub_o = is_sub ? alu_op1_i + ~alu_op2_i +32'b00000000_00000000_00000000_00000001 : alu_op1_i + alu_op2_i;
   //assign is_sub = (alu_ctr == `ALU_SUB)? 1'b1 : 1'b0;
   assign math_op1 = {alu_op1_i[31], alu_op1_i[31:0]};
   assign math_op2 = {alu_op2_i[31], alu_op2_i[31:0]};
   
   assign is_equal = (alu_ctr == `ALU_SUB) && (alu_op1_i == alu_op2_i);

   assign alu_o = (alu_ctr == `ALU_OR)? (alu_op1_i | alu_op2_i):
		  (alu_ctr == `ALU_ADD)? add_re[31:0]:
		  (alu_ctr == `ALU_SUB)? sub_re[31:0] : 32'bx;
   assign add_re = math_op1 + math_op2;
   assign sub_re = math_op1 + ~math_op2 + 33'b0_00000000_00000000_00000000_00000001;
   
//   always @(*)
//     begin
//	case(alu_ctr)
//	  `ALU_OR: alu_o = alu_or_o;
//	  default: alu_o = alu_add_sub_o;
//	endcase // case (alu_ctr)
//     end
endmodule // ALU
